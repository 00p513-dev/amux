#!/bin/ash

AMUX_IMAGE_NAME="amux-0.1-alpha"

rm -Rf rootfs *.img

mkdir -p rootfs/{boot,dev/pts,etc/opt,home,media,mnt,opt,proc,root,run,srv,sys,tmp,usr/bin,usr/include,usr/lib,usr/local/bin,usr/local/etc,usr/local/games,usr/local/lib,usr/local/man,usr/local/sbin,usr/local/share,usr/local/src,usr/bin,usr/share/man,usr/share/misc,var/cache,var/lib,var/local,var/lock,var/log,var/opt,var/run,var/spool,var/tmp}

ln -s /usr/bin rootfs/bin
ln -s /usr/bin rootfs/sbin
ln -s /usr/bin rootfs/usr/sbin
ln -s /usr/lib rootfs/lib

for i in acpid addshell addgroup adduser adjtimex arch arp arping ash awk base32 base64 basename bc beep blkdiscard blkid blockdev bootchartd brctl bunzip2 bzcat bzip2 cal cat chat chattr chgrp chmod chown chpasswd chpst chroot chrt chvt cksum clear cmp comm conspy cp cpio crond crontab         cryptpw cttyhack cut date dc dd deallocvt delgroup deluser depmod devmem df dhcprelay diff dirname dmesg dnsd dnsdomainname dos2unix dpkg dpkgdeb du dumpkmap dumpleases echo ed egrep eject env envdir envuidgid etherwake expand expr factor fakeidentd fallocate false fatattr fbset fbsplash fdflush fdformat fdisk fgconsole fgrep find findfs flock fold free freeramdisk fsck fsckminix fsfreeze fstrim fsync ftpd ftpget ftpput fuser getopt getty grep groups gunzip gzip halt hd hdparm head hexdump hexedit hostid hostname httpd hush hwclock i2cdetect i2cdump i2cget i2cset         i2ctransfer id ifconfig ifdown ifenslave ifplugd ifup inetd init insmod install ionice iostat ip ipaddr ipcalc ipcrm ipcs iplink ipneigh iproute iprule iptunnel kbdmode kill killall killall5 klogd last less link linux32 linux64 linuxrc ln loadfont loadkmap logger login logname logread         losetup lpd lpq lpr ls lsattr lsmod lsof lspci lsscsi lsusb lzcat lzma lzop makedevs makemime man md5sum mdev mesg microcom mim mkdir mkdosfs mke2fs mkfifo mkfsext2 mkfsminix mkfsvfat mknod mkpasswd mkswap mktemp modinfo modprobe more mount mountpoint mpstat mt mv nameif nanddump nandwrite nbdclient nc netstat nice nl nmeter nohup nologin nproc nsenter nslookup ntpd nuke od openvt partprobe passwd paste patch pgrep pidof ping ping6 pipeprogress pivotroot pkill pmap popmaildir poweroff powertop printenv printf ps pscan pstree pwd pwdx raidautorun rdate rdev readahead readlink readprofile realpath reboot reformime removeshell renice reset resize resume rev rm rmdir rmmod route rpm rpm2cpio rtcwake runinit runparts runlevel runsv runsvdir rx script scriptreplay sed sendmail seq setarch setconsole setfattr setfont setkeycodes setlogcons setpriv setserial setsid setuidgid sh sha1sum sha256sum sha3sum sha512sum showkey shred shuf slattach sleep smemcap softlimit sort split sslclient startstopdaemon stat strings stty su sulogin sum sv svc svlogd svok swapoff swapon switchroot sync sysctl syslogd tac tail tar taskset tc tcpsvd tee telnet telnetd test tftp tftpd time timeout top touch tr traceroute traceroute6 true truncate ts tty ttysize tunctl ubiattach         ubidetach ubimkvol ubirename ubirmvol ubirsvol ubiupdatevol udhcpc udhcpc6 udhcpd udpsvd uevent umount uname unexpand uniq unix2dos unlink unlzma unshare unxz unzip uptime users usleep uudecode uuencode vconfig vi vlock volname w wall watch watchdog wc wget which who whoami whois xargs xxd xz xzcat yes zcat zcip;do
    ln -s /bin/busybox rootfs/usr/bin/$i
done

ln -s /usr/bin/pfetch rootfs/usr/bin/neofetch

cp -pr files/bin/* rootfs/usr/bin/
cp -pr files/boot/* rootfs/boot/
cp -pr files/etc/* rootfs/etc/

cp -pr musl/usr/* rootfs/usr/
cp -pr musl/lib/* rootfs/usr/lib

cp -pr openssl/* rootfs/usr/

chmod 0755 rootfs/usr/bin/* &> /dev/null

mkfs.ext4 -d rootfs -b 4096 rootfs.img 64m

#dd if=/dev/zero of=$AMUX_IMAGE_NAME bs=1M count=65
#echo 'type=83' | sudo sfdisk 
